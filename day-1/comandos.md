## Comandos importantes Git dia 1

git clone : Realiza a copia dos arquivos do repositório remoto para a sua maquina.

git status : Retorna as 

git add : Adiciona os arquivos para acompanhar as modificações.

git commit -m "Mensagem": Adiciona as lista de modificações geradas com git add para a HEAD do repositório local.

git push : Publica as alterações comitadas da HEAD na brach remota.

git log: Listas as interações com o repositorio. Comites
	git log -N: lista as ultimas "n" alterações.
	git log --oneline: Retornas a lista de comites em uma linha
	git log --author="nome": Retornas as alterações do author.
	git log --graph --decorate: Traz a arvore de comites e branchs
git pull : Pega os arquivos do server remoto e traz para o repositorio local;
git rm: apaga os arquivos da branch

git checkout <branch> : troca de branch
	git checkout -b nome: cria e troca de branch
git push origin <branch> cria a branch remota

