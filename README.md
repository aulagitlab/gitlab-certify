# Gitlab certify 
```bash
# Pratica para certificação em gitLab.
```
### Day-1
- Entendemos o git
- Entendemos o Gitlab
- Aprendemos os comandos básicos para manipulação de arquivos
- Como criar um repositorio
- Como criar um branch
- Como criar um Merge Request
- Como criar um grupo
- Como fazer um merge
- Como adicionar membros ao projeto.